require 'test_helper'

class PostTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
 
 test "Post is invalid without title" do
   post = Post.new :body => "body"
   assert post.invalid?
 end

 test "Post is invalid without body" do
   post = Post.new :title => "title"
   assert post.invalid?
 end

 test "Post is invalid without body and title" do
   post = Post.new
   assert post.invalid?
 end

 test "Post valid with body and title" do
   post = Post.new :title=>"title", :body=>"body"
   assert post.valid?
 end

end
