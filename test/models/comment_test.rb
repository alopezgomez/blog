require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

 test "Comment is invalid without post_id" do
   comment = Comment.new :body => "body"
   assert comment.invalid?
 end

 test "Comment is invalid without body" do
   comment = Comment.new :post_id => 1
   assert comment.invalid?
 end

 test "Comment is invalid without body and post_id" do
   comment = Comment.new
   assert comment.invalid?
 end

 test "Comment valid with body and post_id" do
   comment = Comment.new :post_id=>1, :body=>"body"
   assert comment.valid?
 end
end
